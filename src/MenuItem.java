
public class MenuItem {
    private String name;
    private String description;
    private double price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * constructor
     * @param name
     * @param description
     * @param price
     */
    public MenuItem(String name, String description, double price) {
        this.name = name;
        this.description = description;
        this.price = price;
    }

    public String getDescription() {
        return description;

    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) throws Exception {
        //throw an exception if the new price is nonpositive
        if (price < 0) {
            throw new Exception("Price can't be nonpositive");
        }
        this.price = price;
    }
}
