import java.util.InputMismatchException;
import java.util.Scanner;

public class MenuOperations implements Menu.order {
    private static Menu menu = new Menu();
    private static Scanner input = new Scanner(System.in);
    private int m_order_num = 0;

    public static void main(String[] args) {
        MenuOperations operations = new MenuOperations();
        printMenu();
        String option = input.next();
        while (!option.equals("Q") || !option.equals("q")) {
            switch (option) {
                case "A":
                case "a":
                    addItem();
                    break;
                case "G":
                case "g":
                    getItem();
                    break;
                case "R":
                case "r":
                    removeItem();
                    break;
                case "P":
                case "p":
                    printAllItems();
                    break;
                case "S":
                case "s":
                    Size();
                    break;
                case "D":
                case "d":
                    updateDescription();
                    break;
                case "C":
                case "c":
                    updatePrice();
                    break;
                case "O":
                case "o":
                    operations.addToOrder();
                    break;
                case "I":
                case "i":
                    operations.removeFromOrder();
                    break;
                case "V":
                case "v":
                    operations.viewOrder();
                    break;
                default:
                    System.out.println("No such operation");
                    break;
            }
            printMenu();
            option = input.next();
        }
    }
    //update item description by position
    private static void updateDescription() {
        System.out.print("Enter the position:");
        try {
            int pos = input.nextInt();
            try {
                MenuItem item = menu.getItem(pos);
                System.out.print("Enter the new description:");
                String des = input.next();
                item.setDescription(des);
                System.out.println("New description set");
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
            }
        } catch (InputMismatchException e) {
            System.out.println("input must be int");
            input.nextLine();
        }
    }
    //remove item by name
    private static void removeItem() {
        System.out.print("Enter the Name:");
        String name = input.next();
        menu.removeItem(name);
    }
    //print all items
    private static void printAllItems() {
        System.out.println("MENU:");
        printTitle();
        menu.printAllItems();
    }
    //print main menu
    private static void printMenu() {
        System.out.print("Main menu:\n\n" +
                "A) Add Item\n" +
                "G) Get Item\n" +
                "R) Remove Item\n" +
                "P) Print All Items\n" +
                "S) Size\n" +
                "D) Update description\n" +
                "C) Update price\n" +
                "O) Add to order\n" +
                "I) Remove from order\n" +
                "V) View order\n" +
                "Q) Quit\n\n" +
                "Select an operation:"
        );
    }
    //add new item
    private static void addItem() {
        System.out.print("Enter the name:");
        String name = input.next();
        System.out.print("Enter the description:");
        String description = input.next();
        System.out.print("Enter the price:");
        String price = input.next();
        double d_price;
        try {
            d_price = Double.parseDouble(price);
        } catch (Exception e) {
            System.out.println("price must be double");
            return;
        }
        System.out.print("Enter the position:");
        try {
            int pos = input.nextInt();
            try {
                menu.addItem(new MenuItem(name, description, d_price), pos);
                System.out.println("Added “" + name + ": " + description + "” for $" + d_price + " at position " + pos);
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
            }
        } catch (InputMismatchException e) {
            System.out.println("input must be int");
            input.nextLine();
        }
    }
    //update price of item by name
    private static void updatePrice() {
        System.out.print("Enter the name of the item:");
        String name = input.next();
        try {
            MenuItem item = menu.getItemByName(name);
            System.out.print("Enter the new price:");
            String price = input.next();
            double d_price;
            try {
                d_price = Double.parseDouble(price);
                try {
                    item.setPrice(d_price);
                    System.out.println("Changed the price of “" + name + "” to $" + price);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

            } catch (Exception e) {
                System.out.println("price must be double");
            }
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }
    //print number of menu
    private static void Size() {
        System.out.println("There are " + menu.size() + " items in the menu");
    }
    //get item by position
    public static void getItem() {
        System.out.print("Enter the position:");
        try {
            int pos = input.nextInt();
            try {
                MenuItem item = menu.getItem(pos);
                printTitle();
                System.out.println(String.format("%-9d%-20s%-70s%.2f", pos, item.getName(), item.getDescription(), item.getPrice()));
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
            }
        } catch (InputMismatchException e) {
            System.out.println("input must be int");
            input.nextLine();
        }
    }
    //add item to order
    @Override
    public void addToOrder() {
        System.out.print("Enter position of item to add to order:");
        try {
            int pos = input.nextInt();
            try {
                MenuItem item = menu.getItem(pos);
                order_arr[m_order_num] = item;
                m_order_num++;
                System.out.println("Added “" + item.getName() + "” to order");
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
            }
        } catch (InputMismatchException e) {
            System.out.println("input must be int");
            input.nextLine();
        }
    }
    //remove item of order
    @Override
    public void removeFromOrder() {
        System.out.print("Enter the position:");
        try {
            int pos = input.nextInt();
            if (pos <= 0 || pos > m_order_num) {
                System.out.println("invalid position");
                return;
            }
            MenuItem item = order_arr[pos - 1];
            for (int i = m_order_num; i > pos - 1; i--) {
                order_arr[i - 1] = order_arr[i];
            }
            System.out.println("Removed “" + item.getName() + "” from order");
            m_order_num--;
        } catch (InputMismatchException e) {
            System.out.println("input must be int");
            input.nextLine();
        }

    }
    //print all orders
    @Override
    public void viewOrder() {
        System.out.println("ORDER:");
        printTitle();
        for (int i = 0; i < m_order_num; i++) {
            MenuItem item = order_arr[i];
            System.out.println(String.format("%-9d%-20s%-70s%.2f", i + 1, item.getName(), item.getDescription(), item.getPrice()));
        }
    }
    //print format title
    private static void printTitle() {
        System.out.println(
                "#    Name        Description                             Price\n" +
                        "-------------------------------------------------------------------------------------------------------");
    }
}
