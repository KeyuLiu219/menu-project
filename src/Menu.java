
public class Menu {
    //max num
    public static final int MAX_ITEMS = 50;
    private MenuItem[] m_MenuItems;
    private int m_number;

    public Menu() {
        m_MenuItems = new MenuItem[MAX_ITEMS];
        m_number = 0;
    }
    //the interface for order
    public interface order {
        MenuItem[] order_arr = new MenuItem[MAX_ITEMS];

        void addToOrder();

        void removeFromOrder();

        void viewOrder();
    }
    //return size of menu
    public int size() {
        return m_number;
    }
    //add a new item
    public void addItem(MenuItem item, int position) {
        if (m_number == MAX_ITEMS) {
            System.out.println("There is no more room inside of the Menu to store the new MenuItem object");
            return;
        }
        //position is invalid
        if (position < 1 || position > (m_number + 1)) {
            throw new IllegalArgumentException("The position in the Menu and not the position inside the array");
        }
        for (int i = m_number; i > position - 1; i--) {
            m_MenuItems[i] = m_MenuItems[i - 1];
        }
        m_MenuItems[position - 1] = item;
        m_number++;
    }
    //remove a item by position
    public void removeItem(int position) {
        if (position < 1 || position > (m_number + 1)) {
            throw new IllegalArgumentException("The position in the Menu and not the position inside the array");
        }
        for (int i = m_number; i > position - 1; i--) {
            m_MenuItems[i - 1] = m_MenuItems[i];
        }
        m_number--;
    }
    //remove a item by menuItem name
    public void removeItem(String name) {
        int i;
        for (i = 0; i < m_number; i++) {
            if (m_MenuItems[i].getName().equals(name)) {
                break;
            }
        }
        if (i == m_number) {
            System.out.println("no such item");
            return;
        }
        removeItem(i+1);
        System.out.println("Removed “"+name+"”");
    }
    //get item by position
    public MenuItem getItem(int position) {
        if (position < 1 || position > m_number) {
            throw new IllegalArgumentException("The position in the Menu and not the position inside the array");
        }
        return m_MenuItems[position - 1];
    }
    //get item by name
    public MenuItem getItemByName(String name) {
        MenuItem temp = null;
        for (int i = 0; i < m_number; i++) {
            if (m_MenuItems[i].getName().equals(name)){
                temp = m_MenuItems[i];
            }
        }
        //find that item
        if(temp != null){
            return temp;
        }
        //no such item by the name
        throw new IllegalArgumentException("The given item does not exist in this Menu");
    }
    //print all items
    public void printAllItems() {
        for (int i = 0; i < m_number; i++) {
            MenuItem item = m_MenuItems[i];
            System.out.println(String.format("%-9d%-20s%-69s%.2f", i + 1, item.getName(), item.getDescription(), item.getPrice()));
        }
    }
}
